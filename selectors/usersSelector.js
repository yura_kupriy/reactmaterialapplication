import { createSelector } from 'reselect';

const getUserState = state => state.users.user;
const getUsersState        = state => state.users.users;
const getSearchUsersState  = state => state.users.searchUsers;
const getSelectedUserState = state => state.users.selectedUser;

export const getCurrentUser = createSelector(getUserState, user => user);

export const getSelectedUser = createSelector(
	getSelectedUserState,
	user => user
);

export const getUsersList           = createSelector(
	getUsersState,
	users => users
);
export const getSearchUsersList     = createSelector(
	getSearchUsersState,
	searchUsers => searchUsers
);
export const getSearchableUsersList = createSelector(
	[
		getUsersState,
		getSearchUsersState
	],
	(users, searchUsers) => {
		return searchUsers.length >= 1 ? searchUsers : users;
	}
);