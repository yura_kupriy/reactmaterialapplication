import PropTypes from 'prop-types';
import { UserKeys } from '../constants/user/userKeys';
import { Gender } from '../constants/gender';

const { ID, NAME, EMAIL, PHONE, GENDER, LAST_NAME, CREATED_AT, PROFILE_PICTURE_URL } = UserKeys;

export const user = PropTypes.shape({
  [ID]: PropTypes.string.isRequired,
  [NAME]: PropTypes.string.isRequired,
  [PHONE]: PropTypes.string,
  [EMAIL]: PropTypes.string.isRequired,
  [GENDER]: PropTypes.oneOf([Gender.MALE, Gender.FEMALE]),
  [LAST_NAME]: PropTypes.string.isRequired,
  [CREATED_AT]: PropTypes.instanceOf(Date),
  [PROFILE_PICTURE_URL]: PropTypes.string.isRequired
});

export const users = PropTypes.arrayOf(PropTypes.shape(user));
