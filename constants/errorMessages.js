import upperFirst from 'lodash/upperFirst';

export const ErrorMessages = {
	EMAIL_REQUIRED: 'Email is required',
	EMAIL_INVALID : 'Invalid email address',
	INPUT_REQUIRED: input => `${upperFirst(input)} is required`
};