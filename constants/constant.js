export const EMPTY_STRING = '';
export const REGEXP_EMPTY_STRING = / /g;
export const EMAIL_REGEXP = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
export const USERS_API_URL = `http://584fc4d576354b1200e88750.mockapi.io/api/v1/users`;
