export const UserKeys = {
  ID: 'id',
  NAME: 'name',
  PHONE: 'phone',
  EMAIL: 'email',
  GENDER: 'gender',
  LAST_NAME: 'surname',
  CREATED_AT: 'created_at',
  PROFILE_PICTURE_URL: 'avatar'
};
