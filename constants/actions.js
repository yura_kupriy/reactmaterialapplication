/* USERS ACTIONS*/
export const GET_USER        = `GET_USER`;
export const SAVE_USER       = `SAVE_USER`;
export const GET_USERS       = `GET_USERS`;
export const UPDATE_USER     = `UPDATE_USER`;
export const REMOVE_USER     = `REMOVE_USER`;
export const SEARCH_USERS    = `SEARCH_USERS`;
export const SET_ACTIVE_USER = `SET_ACTIVE_USER`;