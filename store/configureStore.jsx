import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import invariant from 'redux-immutable-state-invariant';
import * as actionCreators from '../constants/actions';

export default function configureStore() {
  const composeEnhancers = composeWithDevTools({ actionCreators });
  const store = createStore(
      rootReducer,
      composeEnhancers(
          applyMiddleware(
              thunk,
              invariant()
          )
      )
  );
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers');
      store.replaceReducer(nextReducer);
    });
  }
  return store;
}
