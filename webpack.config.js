module.exports = {
	context: __dirname,
	entry  : ['./src/index.jsx', './src/main.css', './src/index.html'],
	
	output: {
		path    : __dirname + '/static',
		filename: 'bundle.js'
	},
	
	module: {
		preLoaders: [
			//Eslint loader
			{test: /\.jsx?$/, exclude: /node_modules/, loader: 'eslint-loader'}
		],
		loaders   : [
			{
				test  : /\.html$/,
				loader: 'file?name=[name].[ext]'
			},
			{
				test  : /\.css$/,
				loader: 'file?name=[name].[ext]'
			},
			{
				test   : /\.jsx?$/,
				exclude: /node_modules/,
				loaders: ['babel-loader']
			}
		]
	},
	
	resolve: {
		extensions: ['', '.js', '.jsx']
	},
	
	eslint: {
		configFile: './.eslintrc'
	},
	
	devServer: {
		open              : true,
		port              : 3300,
		watch             : true,
		watchOptions      : {
			poll            : 1000,
			aggregateTimeout: 300
		},
		inline            : true,
		colors            : true,
		historyApiFallback: true
	}
};

