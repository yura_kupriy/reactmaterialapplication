import React from 'react';
import { connect } from 'react-redux';
import getUsers from '../actions/users/getAllUsers';
import searchInUsersByKey from '../actions/users/searchInUsers';
import { UserKeys } from '../constants/user/userKeys';
import { getSearchableUsersList } from '../selectors/usersSelector';
import UserList from '../components/user/UserList';
import UserAddButton from '../components/user/UserAddButton';
import UsersSearchInput from '../components/user/UsersSearchInput';
import UsersSearchContainer from '../components/user/UsersSearchContainer';
import UserAddButtonContainer from '../components/user/UserAddButtonContainer';

const {NAME, LAST_NAME} = UserKeys;

const MainSection = ({users, getUsers, searchInUsers}) => {
	return (
		<section className="main">
			<UsersSearchContainer>
				<UsersSearchInput
					onChange={event => searchInUsers(event.target.value)}
				/>
			</UsersSearchContainer>
			<UserList users={users}/>
			<UserAddButtonContainer>
				<UserAddButton/>
			</UserAddButtonContainer>
		</section>
	);
};

const mapStateToProps = state => ({
	users: getSearchableUsersList(state)
});

const mapDispatchToProps = dispatch => {
	dispatch(getUsers());

	return () => ({
		searchInUsers: value => dispatch(
			searchInUsersByKey([NAME, LAST_NAME], value)
		)
	});
};

export default connect(mapStateToProps, mapDispatchToProps)(MainSection);
