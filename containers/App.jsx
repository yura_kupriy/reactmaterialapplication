import React from 'react';
import Header from './Header';
import Routing from '../routes/Routing';
import {BrowserRouter as Router} from 'react-router-dom';

// For Customization Options, edit  or use
// './src/material_ui_raw_theme_file.jsx' as a template.
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import theme from '../src/material_ui_raw_theme_file'

const App = () => {
  return (
    <Router>
      <MuiThemeProvider muiTheme={theme}>
        <div>
          <Header/>
          <Routing/>
        </div>
      </MuiThemeProvider>
    </Router>
  );
};

export default App;
