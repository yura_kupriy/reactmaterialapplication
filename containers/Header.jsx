import React from 'react';
import AppBar from 'material-ui/AppBar';

const Header = () => {
	return (
		<header className="header">
			<AppBar title="Users application"/>
		</header>
	);
};

export default Header;
