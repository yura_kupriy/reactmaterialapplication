import {SEARCH_USERS} from '../../constants/actions';

const searchInUsersByKey = (keys, value) => ({
    type: SEARCH_USERS,
    payload: { keys, value }
});

export default searchInUsersByKey;