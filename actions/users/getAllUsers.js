import {responseToJson} from '../../helpers/helpers';
import {GET_USERS} from '../../constants/actions';
import {USERS_API_URL} from '../../constants/constant';


const getUsers = () => dispatch => {
  return fetch(USERS_API_URL)
    .then(responseToJson)
    .then(users =>
      dispatch({
        type: GET_USERS,
        success: true,
        payload: users
      }))
    .catch(() => dispatch({
      type: GET_USERS,
      success: false,
      payload: []
    }));
};

export default getUsers;
