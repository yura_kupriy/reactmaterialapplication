import { UPDATE_USER } from '../../constants/actions';
import { UserKeys } from '../../constants/user/userKeys';
import { USERS_API_URL } from '../../constants/constant';

const updateUser = (updatedUser) => dispatch => {
	fetch(`${USERS_API_URL}/${updatedUser[UserKeys.ID]}`, {
		method: 'PUT',
		body  : JSON.stringify(updatedUser),
		headers: new Headers({'Content-Type': 'application/json'})
	})
		.then(response => response.json())
		.then((updatedUser) => {
			dispatch({
				type   : UPDATE_USER,
				payload: updatedUser
			});
		})
		.catch(console.log.bind(console));
};

export default updateUser;