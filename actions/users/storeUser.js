import saveUser from './saveUser';
import updateUser from './updateUser';
import {UserKeys} from '../../constants/user/userKeys';

const {ID} = UserKeys;

const storeUser = user => dispatch => {
	if (user[ID]) {
		dispatch(updateUser(user));
	}
	else {
		dispatch(saveUser(user));
	}
};

export default storeUser;