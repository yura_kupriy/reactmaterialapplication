import {SET_ACTIVE_USER} from '../../constants/actions';

const setActiveUser = user => ({
    type: SET_ACTIVE_USER,
    payload: user
});

export default setActiveUser;