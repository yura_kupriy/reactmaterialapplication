import { responseToJson } from '../../helpers/helpers';
import { GET_USER } from '../../constants/actions';
import { USERS_API_URL } from '../../constants/constant';

const getUser = (id) => dispatch => {
	if (id > 1) {
		return fetch(`${USERS_API_URL}/${id}`)
			.then(responseToJson)
			.then(user =>
				dispatch({
					type   : GET_USER,
					success: true,
					payload: user
			}))
			.catch(() => dispatch({
				type   : GET_USER,
				success: false,
				payload: {}
			}));
	}
};

export default getUser;
