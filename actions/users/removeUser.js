import { REMOVE_USER } from '../../constants/actions';
import { USERS_API_URL } from '../../constants/constant';

const removeUser = (id) => dispatch => {
  fetch(`${USERS_API_URL}/${id}`, {method: 'delete'})
    .then(() => {
      dispatch({
				type   : REMOVE_USER,
				payload: id
			});
    });
};

export default removeUser;