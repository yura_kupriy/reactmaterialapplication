import { SAVE_USER } from '../../constants/actions';
import { USERS_API_URL } from '../../constants/constant';

const saveUser = (createdUser) => dispatch => {
	fetch(`${USERS_API_URL}`, {
		method : 'POST',
		body   : JSON.stringify(createdUser),
		headers: new Headers({'Content-Type': 'application/json'})
	})
		.then(response => response.json())
		.then((storedUser) => {
			dispatch({
				type   : SAVE_USER,
				payload: storedUser
			});
		})
		.catch(console.log.bind(console));
};

export default saveUser;