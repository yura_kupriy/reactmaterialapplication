import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import { routes } from './router.configuration';

export default () => (
	<Switch>
		{routes.map((route, i) => (
			<Route
				key={i}
				{...route}
			/>
		))}
		<Redirect exact from="*" push to="/"/>
	</Switch>
);
