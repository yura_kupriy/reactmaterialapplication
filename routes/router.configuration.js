import MainSection from '../containers/MainSection';
import UserFormWrapper from '../components/user/UserFormWrapper';

export const routes = [
	{
		path     : '/',
		component: MainSection,
		exact    : true
	},
	{
		path     : '/users/:id',
		component: UserFormWrapper,
		exact    : true
	}
];