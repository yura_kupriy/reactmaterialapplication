import join from 'lodash/fp/join';
import pick from 'lodash/fp/pick';
import keyBy from 'lodash/fp/keyBy';
import values from 'lodash/fp/values';
import reject from 'lodash/fp/reject';
import pipe from 'lodash/fp/pipe';
import { UserKeys } from '../../constants/user/userKeys';
import * as ACTIONS from '../../constants/actions';
import {EMPTY_STRING, REGEXP_EMPTY_STRING} from '../../constants/constant';
import { isMatch } from '../../helpers/helpers';

const {ID} = UserKeys;

const initialState = {
  user: null,
  users: [],
  usersById: null,
  searchUsers: [],
  selectedUser: null
};

const users = (state = initialState, action) => {
  const states = {
    [ACTIONS.GET_USER]: (user) => ({
      ...state, user
    }),
    [ACTIONS.GET_USERS]: (users = []) => ({
      ...state,
      user: initialState.user,
      users: users,
      usersById: pipe(keyBy(ID))(users)
    }),
    [ACTIONS.SAVE_USER]: user => ({
      ...state,
      users: [...state.users, user]
    }),
    [ACTIONS.UPDATE_USER]: (user) => ({
      ...state,
      user,
      users: state.users.map(_user => _user[ID] === user[ID] ? user : _user)
    }),
    [ACTIONS.SET_ACTIVE_USER]: (selectedUser = {}) => ({
      ...state, selectedUser
    }),
    [ACTIONS.SEARCH_USERS]: ({keys = [], value = ''}) => ({
      ...state,
      searchUsers: state.users.filter(pipe(
        pick(keys),
        values,
        join(EMPTY_STRING),
        isMatch(new RegExp(
          value.replace(REGEXP_EMPTY_STRING, EMPTY_STRING),
          'gi'
        ))
      ))
    }),
    [ACTIONS.REMOVE_USER]: id => ({
      ...state,
      user: null,
      users: pipe(reject([ID, id]))(state.users),
      usersById: pipe(reject([ID, id]), keyBy(ID))(state.users)
    })
  };
  return states[action.type] ? states[action.type](action.payload) : state;
};

export default users;
