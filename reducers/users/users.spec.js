import reducer from './users';
import * as types from '../../constants/actions';

const initialState = {
  user       : null,
  users      : [],
  searchUsers: [],
  usersById  : null
};

describe('Users reducer', () => {
  it('should have initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should get users', () => {//@TODO create request mock
    const users  = [{id: 1, placeId: 2}, {id: 2, placeId: null}];
    const action = {
      type   : types.GET_USERS,
      payload: users
    };
    expect(reducer({}, action)).toEqual({
                                          users,
                                          usersById: {
                                            1: {id: 1, placeId: 2},
                                            2: {id: 2, placeId: null}
                                          }
                                        });
  });
  it('should set active user', () => {
    const user   = {a: 'a'};
    const action = {
      type   : types.SET_ACTIVE_USER,
      payload: user
    };
    expect(reducer({user: null}, action)).toEqual({user});
  });
  it('should remove user', () => {
    const user   = {id: 'a'};
    const users  = [user, {id: 'b'}];
    const action = {
      type   : types.REMOVE_USER,
      payload: user.id
    };
    expect(reducer({users}, action)).toEqual({
                                               user     : null,
                                               users    : [{id: 'b'}],
                                               usersById: {
                                                 'b': {id: 'b'}
                                               }
                                             })
    ;
  });
  it('should find user where term exist in key', () => {
    const term       = 'John';
    const userToFind = {name: 'John'};
    const users      = [userToFind, {name: 'Joseph'}];
    const action     = {
      type   : types.SEARCH_USERS,
      payload: {
        keys : ['name'],
        value: term
      }
    };
    
    expect(reducer({users}, action)).toEqual({
                                               users,
                                               searchUsers: [userToFind]
                                             });
  });
});
