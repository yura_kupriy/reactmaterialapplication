import cond from 'lodash/cond';
import isNil from 'lodash/isNil';
import negate from 'lodash/negate';
import equals from 'lodash/fp/equals';
import constant from 'lodash/constant';
import stubTrue from 'lodash/stubTrue';
import { EMAIL_REGEXP } from '../constants/constant';
import { ErrorMessages } from '../constants/errorMessages';
import { Gender } from '../constants/gender';
import { UserKeys } from '../constants/user/userKeys';

const { MALE, FEMALE } = Gender;
const { NAME, EMAIL, GENDER, LAST_NAME } = UserKeys;

export const REQUIRED_FIELDS = [NAME, LAST_NAME, EMAIL, GENDER];

const isEmpty = value => isNil(value) || String(value).trim().length === 0;
const isValidEmail = email => EMAIL_REGEXP.test(email);
const isInvalidEmail = negate(isValidEmail);

const getEmailErrors = cond([
	[isEmpty       , constant(ErrorMessages.EMAIL_REQUIRED)],
	[isInvalidEmail, constant(ErrorMessages.EMAIL_INVALID)],
	[stubTrue      , constant(null)]
]);

export const validateUserForm = (values) => REQUIRED_FIELDS.reduce((error, input) => ({
	...error,
	[input] : isEmpty(values[input]) ? ErrorMessages.INPUT_REQUIRED(input) : null,
	[EMAIL] : getEmailErrors(values[EMAIL]),
	[GENDER]: [MALE, FEMALE].some(equals(values[GENDER])) ? null : ErrorMessages.INPUT_REQUIRED(GENDER)
}), {});
