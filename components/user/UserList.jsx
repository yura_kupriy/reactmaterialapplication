import React from 'react';
import { List } from 'material-ui/List';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as userShapes from '../../shapes/user';
import removeUser from '../../actions/users/removeUser';
import UserListItem from './UserListItem';

const propTypes = {
	users: PropTypes.oneOfType([userShapes.users, PropTypes.array]).isRequired,
	removeUser: PropTypes.func.isRequired
};

const UserList = ({ users, removeUser }) => {
	return (
		<List>
			{users.map((user) => (
				<UserListItem
					key={user.id}
					user={user}
					onRemove={removeUser}
				/>
			))}
		</List>
	);
};

UserList.propTypes = propTypes;

export default connect(null, { removeUser })(UserList);
