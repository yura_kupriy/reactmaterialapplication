import React from 'react';
import { ContentAdd } from 'material-ui/svg-icons/index';
import { FloatingActionButton } from 'material-ui';
import { withRouter } from 'react-router-dom';

const UserAddButton = ({ history }) => {
  return (
    <FloatingActionButton secondary={true} onTouchTap={() => {
      history.push({
        id: -1,
        pathname: 'users/-1'
      })
    }}>
      <ContentAdd/>
    </FloatingActionButton>
  );
};

export default withRouter(UserAddButton);
