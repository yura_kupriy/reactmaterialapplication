import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Divider, IconButton, IconMenu, MenuItem } from 'material-ui';
import { ListItem } from 'material-ui/List';
import { grey400 } from 'material-ui/styles/colors';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import * as userShapes from '../../shapes/user';
import { withRouter } from 'react-router-dom';
import { UserKeys } from '../../constants/user/userKeys';
const {ID, NAME, LAST_NAME, PROFILE_PICTURE_URL} = UserKeys;

const propTypes = {
	user: userShapes.user.isRequired,
	history: PropTypes.object,
	onRemove: PropTypes.func.isRequired
};

const UserListItem = ({ user, history, onRemove }) => {

	const iconButtonElement = (
		<IconButton
			touch={true}
			tooltip="Actions"
			tooltipPosition="bottom-left"
		>
			<MoreVertIcon color={grey400}/>
		</IconButton>
	);

	const rightIconMenu = (
		<IconMenu iconButtonElement={iconButtonElement}>
			<MenuItem onTouchTap={() => history.push({
				id: user[ID],
				pathname: `users/${user[ID]}`
			})}>Edit</MenuItem>
			<MenuItem onTouchTap={() => onRemove(user[ID])}>Delete</MenuItem>
		</IconMenu>
	);

	return (
		<div>
			<ListItem
				primaryText={user[LAST_NAME]}
				secondaryText={user[NAME]}
				rightIconButton={rightIconMenu}
				leftAvatar={<Avatar src={user[PROFILE_PICTURE_URL]}/>}
			/>
			<Divider inset={true}/>
		</div>
	);
};

UserListItem.propTypes = propTypes;

export default withRouter(UserListItem);
