import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'material-ui';

const propTypes = {
	onChange: PropTypes.func.isRequired
};

const UsersSearchInput = ({onChange}) => {
	return (
			<TextField
				hintText="Search in users"
				floatingLabelText="Search via users"
				onChange={onChange}
			/>
	);
};

UsersSearchInput.propTypes = propTypes;

export default UsersSearchInput;
