import React from 'react';
import PropTypes from 'prop-types';
import isObject from 'lodash/isObject';
import defaults from 'lodash/defaults';
import { RadioButton, RaisedButton } from 'material-ui';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import { Gender } from '../../constants/gender';
import { UserKeys } from '../../constants/user/userKeys';
import { getCurrentUser } from '../../selectors/usersSelector';
import { REQUIRED_FIELDS, validateUserForm } from '../../validation/userFormValidation';
import { MaterialRadioGroup } from '../inputs/MaterialRadioGroup';
import { MaterialTextField } from '../inputs/MaterialTextField';
const {ID, NAME, EMAIL, GENDER, LAST_NAME} = UserKeys;

const style = {
	margin: 5
};

const propTypes = {
	onCancel: PropTypes.func.isRequired,
	onSubmit: PropTypes.func.isRequired
};

let UserForm = ({ onCancel, handleSubmit /* ReduxForm convention */ }) => {
	return (
		<div>
			<form name="user">
				<Field
					name={NAME}
					style={style}
					label="First Name"
					component={MaterialTextField}
				/>
				<Field
					name={LAST_NAME}
					label="Last Name"
					style={style}
					component={MaterialTextField}
				/>
				<Field
					name={EMAIL}
					style={style}
					label="Email"
					component={MaterialTextField}
				/>
				<Field name={GENDER} component={MaterialRadioGroup}>
					<RadioButton
						value={Gender.FEMALE}
						label="Female"
						style={style}
					/>
					<RadioButton
						value={Gender.MALE}
						label="Male"
						style={style}
					/>
				</Field>
				<RaisedButton
					icon={<ArrowBack/>}
					label="CANCEL"
					labelPosition="after"
					secondary={true}
					style={style}
					onTouchTap={() => onCancel()}
				/>
				<RaisedButton
					label="SAVE"
					primary={true}
					style={style}
					onTouchTap={handleSubmit}
				/>
			</form>
		</div>
	);
};

const mapStateToProps = (state) => ({
  initialValues: defaults(
    {},
    isObject(getCurrentUser(state)) ? getCurrentUser(state) : {},
    [NAME, EMAIL, GENDER, LAST_NAME].reduce((form, input) => ({
      ...form,
      [ID]: null,
      [input]: ''
    }), {})
  )
});

UserForm.propTypes = propTypes;

UserForm = reduxForm({
	form  : 'userForm',
	fields: REQUIRED_FIELDS,
	validate: validateUserForm,
	enableReinitialize: true
})(UserForm);

export default connect(mapStateToProps)(UserForm);
