import React from 'react';
import { UserKeys } from '../../constants/user/userKeys';
import { Avatar } from 'material-ui';
import * as userShapes from '../../shapes/user';

const {PROFILE_PICTURE_URL} = UserKeys;

const styles = {
  bigAvatar: {
    margin: 10,
    width: 200,
    height: 200
  }
};

const propTypes = {
  user: userShapes.user
};

const UserAvatar = ({ user }) => (
  user &&
  <Avatar
    src={user[PROFILE_PICTURE_URL]}
    alt="User preview image"
    style={styles.bigAvatar}
  />
);

UserAvatar.propTypes = propTypes;

export default UserAvatar;
