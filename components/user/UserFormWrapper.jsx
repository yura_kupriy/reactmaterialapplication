import React  from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import getUser from '../../actions/users/getOneUser';
import storeUser from '../../actions/users/storeUser';
import * as userShapes from '../../shapes/user';
import { getCurrentUser} from '../../selectors/usersSelector';
import UserForm from './UserForm';
import UserAvatar from "./UserAvatar";

const propTypes = {
  user: userShapes.user,
  history: PropTypes.object.isRequired,
	onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

const UserFormWrapper = ({user, history, onSubmit, onCancel}) => {
	return (
	  <div style={{width: 300, margin: '0 auto'}}>
			<h2 style={{textAlign: 'center'}}>User Form</h2>
      <UserAvatar user={user} />
			<UserForm
				user={user}
				onSubmit={onSubmit}
				onCancel={onCancel}
			/>
		</div>
	);
};

const mapStateToProps = (state) => ({
	user: getCurrentUser(state)
});

const mapDispatchToProps = (dispatch, ownProps) => {
	dispatch(getUser(get(ownProps, 'history.location.id')));

	return () => ({
		onSubmit: user => {
			dispatch(storeUser(user));
			ownProps.history.goBack();
		},
		onCancel: () => {
			ownProps.history.goBack();
		}
	});
};

UserFormWrapper.propTypes = propTypes;

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withRouter(UserFormWrapper));
