import styled from 'styled-components';

export default styled.span`
	margin: 5px;
  font-size: 12px;
  color: rgb(244, 67, 54);
`;