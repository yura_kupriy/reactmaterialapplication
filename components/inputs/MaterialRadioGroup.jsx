import React from 'react';
import { RadioButtonGroup } from 'material-ui';
import ErrorMessage from '../general/ErrorMessage';

export const MaterialRadioGroup = ({ input, meta: { touched, error }, ...rest }) => (
	<div>
		<RadioButtonGroup
			{...input}
			{...rest}
			valueSelected={input.value}
			errorText={touched && error}
			onChange={(event, value) => input.onChange(value)}
		/>
		<ErrorMessage>{touched && error}</ErrorMessage>
	</div>
);
