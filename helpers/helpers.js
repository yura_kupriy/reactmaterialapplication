import {curry, hasIn} from 'lodash';

export const isMatch = curry((regexp, value) => value.match(regexp));

export const responseToJson = response => {
  return hasIn(response, 'json')
    ? response.json()
    : response;
};

export const joinKeysByValues = object => {
  return Object
    .keys(object)
    .filter(key => object[key])
    .join(' ');
};
