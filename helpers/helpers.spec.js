import * as helpers from './helpers';
describe('Helpers', () => {
  it('should build string by positive value condition in object', () => {
    expect(helpers.joinKeysByValues({'include': true, 'exclude': false})).toEqual('include');
  });
});
